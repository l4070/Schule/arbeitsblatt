# Kollaborative Dokumentenerstellung mit LaTeX

Workshop im Rahmen der INFOS2021 https://dl.gi.de/handle/20.500.12116/36970

Marei Peischl <marei@peitex.de> & André Hilbig <hilbig@uni-wuppertal.de>

## Arbeitsblatt Demo

# Struktur dieses Verzeichnisses

- ab01_schadden.md eigentliches Arbeitsblatt
- template-marterial-arbeitsblatt.tex: Template Datei für Arbeitsblätter
- code.tex: Einstellungen für das Tikz Bild
- tikz-skizze-teelicht-schatten.tex: Eigentliches Bild

# Das Projekt lokal bauen

Vorraussetzung ist eine aktuelle TeX Live Installation mit latexmk (https://www.tug.org/texlive/acquire-netinstall.html) unter Windows ist ggf. die Installation von perl notwendig.

Anschließend genügt es im Basisverzeichnis `latexmk -r latex-markdown-templates/CI.rc` auszuführen