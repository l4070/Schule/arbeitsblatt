---
template: material-arbeitsblatt
title: Schatten
author: Max Muster
group: 6
subject: Ph
preambleconfig: code.tex
---

# Material

Holt euch die folgenden Materialien ab:
- Teelichter
- Rolle
- Schirm

# Aufbau

*Hinweis:* Zündet die Teelichter erst an, nachdem ihr alles aufgebaut habt.

\input{tikz-skizze-teelicht-schatten}

# Durchführung

Beobachtet das Bild auf dem Schirm. Fertigt eine Skizze dazu an.

Wiederholt den Versuch mit zwei (drei) Teelichtern nebeneinander.
